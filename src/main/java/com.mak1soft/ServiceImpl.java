package com.mak1soft;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;

@Component
public class ServiceImpl implements Service {

    @Autowired
    private AppConfig appConfig;

    @Override
    public String[] pingUrl() {
        final String[] commands = {"bash", "-c", "ping " +
                appConfig.getPingUrl() + " -c 2"};
        String std[] = {"", ""};

        try {
            Process process = Runtime.getRuntime().exec(commands);

            BufferedReader stdInput = new BufferedReader(new
                    InputStreamReader(process.getInputStream()));

            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(process.getErrorStream()));

            String stdInputLine;
            while ((stdInputLine = stdInput.readLine()) != null) {
                std[0] += stdInputLine + "\n";
            }

            String stdErrorLine;
            while ((stdErrorLine = stdError.readLine()) != null) {
                std[1] += stdErrorLine + "\n";
            }

            stdInput.close();
            stdError.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return std;
    }

    public String readLog() {
        final String logFileName = appConfig.getLogFile();
        String log = "";

        try {
            File logFile = new File(logFileName);
            FileReader fileReader = new FileReader(logFile);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String logLine;
            while ((logLine = bufferedReader.readLine()) != null) {
                log += logLine + "\n";
            }

            bufferedReader.close();
        } catch (FileNotFoundException e) {
            //e.printStackTrace();
            log = "log: unknown file " + logFileName;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return log;
    }

}
