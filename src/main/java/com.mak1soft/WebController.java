package com.mak1soft;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WebController {

    @Autowired
    private Service service;

    @RequestMapping("/ping")
    public String ping(Model model) {
        String[] pingReport = service.pingUrl();
        model.addAttribute("ping", pingReport[0]);
        model.addAttribute("ping_err", pingReport[1]);

        return "ping";
    }

    @RequestMapping("/log")
    public String connect(Model model) {
        String log = service.readLog();
        model.addAttribute("log", log);

        return "log";
    }

}
