package com.mak1soft;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AppConfig {

    @Value("${pingUrl}")
    private String pingUrl;

    @Value("${logFile}")
    private String logFile;

    public String getPingUrl() {
        return pingUrl;
    }

    public String getLogFile() {
        return logFile;
    }

}
